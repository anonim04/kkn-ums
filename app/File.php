<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'name', 'extension','dir'
    ];

    public function activity(){
        return $this->hasOne(Activity::class, 'file_id');
    }
}
