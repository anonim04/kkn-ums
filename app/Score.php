<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
        'activity_id', 'lecturer_id','value'
    ];

    public function activity(){
        return $this->belongsTo(Activity::class, 'activity_id');
    }
    public function lecturer(){
        return $this->belongsTo(Lecturer::class, 'lecturer_id');
    }
}
