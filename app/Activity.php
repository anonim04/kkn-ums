<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'colleger_id', 'file_id'
    ];

    public function colleger(){
        return $this->belongsTo(Colleger::class, 'colleger_id');
    }
    public function file(){
        return $this->belongsTo(File::class, 'file_id');
    }
    public function score(){
        return $this->hasOne(Score::class, 'activity_id');
    }
}
