<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $fillable = [
        'user_id', 'major_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function major(){
        return $this->belongsTo(Major::class, 'major_id');
    }
    public function score(){
        return $this->hasOne(Major::class, 'score_id');
    }
}
