<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    protected $fillable = [
        'faculty_id', 'code','name'
    ];

    public function faculty(){
        return $this->belongsTo(Faculty::class, 'faculty_id');
    }

    public function Lecturer(){
        return $this->belongsTo(Lecturer::class, 'lecturer_id');
    }
}
