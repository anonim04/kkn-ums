<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lecturer;
use App\Major;
use App\User;
use App\Http\Requests\LecturerRequest;
use Illuminate\Support\Facades\Hash;

class LecturerController extends Controller
{

    public function index()
    {
        $user = Lecturer::with('user')->get();
        return view('lecturer.index',compact('user'));
    }

    public function create()
    {
        $major = Major::with('faculty')->get();
        return view('lecturer.create', compact('major'));
    }

    public function store(LecturerRequest $request)
    {
        $user = new User();
        $user->role = $request->input('role');
        $user->name = $request->input('name');
        $user->registration_number = $request->input('registration_number');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        if($user->save()){
            $lecturer = new Lecturer();
            $lecturer->user_id = $user->id;
            $lecturer->major_id = $request->input('major');
            if($lecturer->save()){
                return redirect(route('lecturer.index'));
            }
        }
    }

    public function edit($id)
    {
        $major = Major::with('faculty')->get();
        $user = User::findOrFail($id);
        return view('lecturer.edit', compact('major','user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'name' => $request->input('name'),
            'registration_number' => $request->input('registration_number'),
            'email' => $request->input('email'),
        ]);
        $lecturer = Lecturer::where('user_id',$user->id)->firstOrFail();
        $lecturer->update([
            'major_id' => $request->input('major'),
        ]);
        return redirect(route('lecturer.index'));
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect(route('lecturer.index'));
    }
}
