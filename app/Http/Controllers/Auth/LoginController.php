<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Lecturer;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo(){
        if (auth()->user()->role == 'colleger') {
            return route('activity.index',['colleger_id'=>auth()->user()->colleger->id]);
        }
        else if (auth()->user()->role == 'lecturer') {
            return route('guide.index',['lecturer_id'=> auth()->user()->lecturer->id]);
        }
        else if (auth()->user()->role == 'admin') {
            return '/home';
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
