<?php

namespace App\Http\Controllers;

use App\Activity;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($colleger_id)
    {
        $activity = Activity::where('colleger_id',$colleger_id)->with('score')->get();
        return view('activity.index', compact('activity','colleger_id'));
    }

    public function store(Request $request)
    {
        $file = new File();
        $file->name = $request->file('document')->getClientOriginalName();
        $file->extension = $request->file('document')->extension();
        $file->dir = 'storage/app/public/document';
        if($request->file('document')->storeAs('public/document', $file->name)){
            $file->save();
            $activity = new Activity();
            $activity->file_id = $file->id;
            $activity->colleger_id = $request->input('colleger');
            $activity->save();
        }
        return redirect(route('activity.index',['colleger_id'=>$request->input('colleger')]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = File::findOrFail($id);
        $activity->delete();
        return redirect(route('activity.index'));
    }
}
