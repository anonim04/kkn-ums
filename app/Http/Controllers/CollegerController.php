<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colleger;
use App\Http\Requests\CollegerRequest;
use App\Major;
use App\User;
use App\Lecturer;
use Illuminate\Support\Facades\Hash;

class CollegerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Colleger::with('user')->get();
        return view('colleger.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $major = Major::with('faculty')->get();
        $lecturer = Lecturer::with('user')->get();
        return view('colleger.create', compact('major','lecturer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollegerRequest $request)
    {
        $user = new User();
        $user->role = $request->input('role');
        $user->name = $request->input('name');
        $user->registration_number = $request->input('registration_number');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        if($user->save()){
            $lecturer = new Colleger();
            $lecturer->user_id = $user->id;
            $lecturer->major_id = $request->input('major');
            $lecturer->lecturer_id = $request->input('lecturer');
            if($lecturer->save()){
                return redirect(route('login'));
            }
        }
    }

    public function edit($id)
    {
        $major = Major::with('faculty')->get();
        $user = User::findOrFail($id);
        $lecturer = Lecturer::with('user')->get();
        return view('colleger.edit', compact('lecturer','major','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'name' => $request->input('name'),
            'registration_number' => $request->input('registration_number'),
            'email' => $request->input('email'),
        ]);
        $lecturer = Colleger::where('user_id',$user->id)->firstOrFail();
        $lecturer->update([
            'major_id' => $request->input('major'),
            'lecturer_id' => $request->input('lecturer'),
        ]);
        return redirect(route('colleger.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect(route('colleger.index'));
    }
}
