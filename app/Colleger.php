<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colleger extends Model
{
    protected $fillable = [
        'user_id', 'major_id','lecturer_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function major(){
        return $this->belongsTo(Major::class, 'major_id');
    }
    public function activity(){
        return $this->hasOne(Activity::class, 'activity_id');
    }
}
