@extends('layouts.app')

@section('content')
    <div class="columns is-marginless is-centered">
        <div class="column">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Register Dosen</p>
                </header>

                <div class="card-content">
                    <form class="register-form" method="POST" action="{{ route('colleger.update',['id'=>$user->id]) }}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="role" value="colleger">

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Name</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="name" type="name" name="name" value="{{ $user->name }}" required autofocus>
                                    </p>

                                    @if ($errors->has('name'))
                                        <p class="help is-danger">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Nomor Induk Pengajar</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="registration_number" type="name" name="registration_number" value="{{ $user->registration_number }}" required autofocus>
                                    </p>

                                    @if ($errors->has('registration_number'))
                                        <p class="help is-danger">
                                            {{ $errors->first('registration_number') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Jurusan</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <div class="select">
                                            <select name="major">
                                                @foreach ($major as $ket=>$value)
                                                    <option value="{{ $value->id }}" {{ $value->id == $user->colleger->major_id ? 'selected':'' }}>{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </p>

                                    @if ($errors->has('major'))
                                        <p class="help is-danger">
                                            {{ $errors->first('major') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Pembimbing</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <div class="select">
                                            <select name="lecturer">
                                                <option disabled selected>Pilih...</option>
                                                @foreach ($lecturer as $ket=>$value)
                                                    <option value="{{ $value->id }}" {{ $value->id == $user->colleger->lecturer_id ? 'selected':'' }}>{{ $value->user->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </p>

                                    @if ($errors->has('lecturer'))
                                        <p class="help is-danger">
                                            {{ $errors->first('lecturer') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">E-mail Address</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="email" type="email" name="email" value="{{ $user->email }}" required autofocus>
                                    </p>

                                    @if ($errors->has('email'))
                                        <p class="help is-danger">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label"></div>

                            <div class="field-body">
                                <div class="field is-grouped">
                                    <div class="control">
                                        <button type="submit" class="button is-primary">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
