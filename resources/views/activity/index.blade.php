@extends('layouts.app')

@section('content')
<!-- Main container -->
<nav class="level">
    <!-- Left side -->
    <div class="level-left">
        <p class="title">Aktifitas</p>
    </div>

    <!-- Right side -->
    <div class="level-right">
        @if (Auth::user()->role == 'colleger')
        <button class="button modal-button is-primary" data-toggle="modal"  data-target="modalInsertFile">
            <span class="icon">
                <i class="fas fa-plus" aria-hidden="true"></i>
            </span>
        </button>
        <div id="modalInsertFile" class="modal modal-fx-3dSlit">
            <div class="modal-background"></div>
                <div class="modal-card">
                    <header class="modal-card-head">
                        <p class="modal-card-title">
                            Upload File
                        </p>
                    </header>
                    <form class="register-form" method="POST" action="{{ route('activity.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <input type="hidden" name="colleger" value="{{ $colleger_id }}">
                        <section class="modal-card-body">
                            <!-- Content ... -->
                            <div class="field">
                                <div class="file is-centered is-boxed is-success has-name" id="file-js-example">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="document" id="document">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                                <i class="fas fa-upload"></i>
                                            </span>
                                        </span>
                                        <span class="file-name">
                                            No File Uploaded
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </section>
                        <footer class="modal-card-foot buttons is-right">
                            <button type="button" class="button button-close-modal" aria-label="close">
                                Cancel
                            </button>
                            <button type="submit" class="button is-primary">Upload</button>
                        </footer>
                    </form>
                </div>
            <button class="modal-close is-large" aria-label="close"></button>
        </div>
        @endif
    </div>
</nav>
    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <th class="has-text-centered">No</th>
                <th class="has-text-centered">Tanggal Upload</th>
                <th class="has-text-centered">Nilai</th>
                @if (Auth::user()->role == 'colleger')
                    <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($activity as $key=>$value)
                <tr>
                    <th class="has-text-centered">{{ $i++ }}</th>
                    <td>{{ $value->created_at }}</td>
                    <td class="has-text-centered">
                        @if (!$value->score()->exists() && Auth::user()->role == 'lecturer')
                            <form class="register-form" method="POST" action="{{ route('guide.store') }}">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" value="{{ $value->id }}" name="activity">
                                <input type="hidden" value="{{ Auth::user()->lecturer->id }}" name="lecturer">
                                <div class="field has-addons">
                                    <div class="control">
                                        <input class="input" type="number" name="value" required>
                                    </div>
                                    <div class="control">
                                        <button class="button is-info" type="submit">
                                            Kirim
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @elseif($value->score()->exists())
                            {{ $value->score->value }}
                        @else
                            -
                        @endif
                    </td>
                    @if (Auth::user()->role == 'colleger')
                        <td>
                            <div class="field is-grouped is-grouped-centered">
                                <button class="button modal-button is-danger" data-toggle="modal"  data-target="modalDeleteFile{{$value->file_id}}">
                                    <span class="icon">
                                        <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                        </td>
                    @endif
                </tr>
                <div id="modalDeleteFile{{$value->file_id}}" class="modal modal-fx-3dSlit">
                    <div class="modal-background"></div>
                        <div class="modal-card">
                            <header class="modal-card-head">
                                <p class="modal-card-title">
                                    Anda Yakin Ingin Menghapus Data Ini ?
                                </p>
                            </header>
                            <form class="register-form" method="POST" action="{{ route('activity.delete',['id'=>$value->file_id]) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <footer class="modal-card-foot buttons is-right">
                                    <button type="button" class="button button-close-modal" aria-label="close">
                                        Cancel
                                    </button>
                                    <button type="submit" class="button is-danger">Hapus</button>
                                </footer>
                            </form>
                        </div>
                    <button class="modal-close is-large" aria-label="close"></button>
                </div>
            @endforeach
        </tbody>
    </table>
@endsection
