@extends('layouts.app')

@section('content')
<!-- Main container -->
<nav class="level">
    <!-- Left side -->
    <div class="level-left">
        <p class="title">Data Dosen</p>
    </div>

    <!-- Right side -->
    <div class="level-right">
        <a class="button is-primary" href="{{ route('lecturer.create') }}">
            <span class="icon">
                <i class="fas fa-plus" aria-hidden="true"></i>
            </span>
        </a>
    </div>
</nav>
    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <th class="has-text-centered">No</th>
                <th class="has-text-centered">Nomor Induk Pengajar</th>
                <th class="has-text-centered">Nama</th>
                <th class="has-text-centered" colspan="2">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($user as $key=>$value)
                <tr>
                    <th class="has-text-centered">{{ $i++ }}</th>
                    <td>{{ $value->user->registration_number }}</td>
                    <td><a href="{{ route('guide.index',['lecturer_id'=>$value->id]) }}">{{ $value->user->name }}</a></td>
                    <td>
                        <div class="field is-grouped is-grouped-centered">
                            <a class="button is-warning" href="{{ route('lecturer.edit', ['id'=>$value->user_id]) }}">
                                <span class="icon">
                                    <i class="fas fa-edit" aria-hidden="true"></i>
                                </span>
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="field is-grouped is-grouped-centered">
                            <button class="button modal-button is-danger" data-toggle="modal"  data-target="modalDeleteFile{{$value->user_id}}">
                                <span class="icon">
                                    <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                </span>
                            </button>
                        </div>
                    </td>
                </tr>
                <div id="modalDeleteFile{{$value->user_id}}" class="modal modal-fx-3dSlit">
                    <div class="modal-background"></div>
                        <div class="modal-card">
                            <header class="modal-card-head">
                                <p class="modal-card-title">
                                    Anda Yakin Ingin Menghapus Data Ini ?
                                </p>
                            </header>
                            <form class="register-form" method="POST" action="{{ route('lecturer.delete',['id'=>$value->user_id]) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <footer class="modal-card-foot buttons is-right">
                                    <button type="button" class="button button-close-modal" aria-label="close">
                                        Cancel
                                    </button>
                                    <button type="submit" class="button is-danger">Hapus</button>
                                </footer>
                            </form>
                        </div>
                    <button class="modal-close is-large" aria-label="close"></button>
                </div>
            @endforeach
        </tbody>
      </table>
@endsection
