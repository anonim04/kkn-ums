@extends('layouts.app')

@section('content')
<!-- Main container -->
<nav class="level">
    <!-- Left side -->
    <div class="level-left">
        <p class="title">List</p>
    </div>
</nav>
    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <th class="has-text-centered">No</th>
                <th class="has-text-centered">Nomor Induk Mahasiswa</th>
                <th class="has-text-centered">Nama</th>
                <th class="has-text-centered">Jurusan</th>
                <th class="has-text-centered" colspan="2">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($colleger as $key=>$value)
                <tr>
                    <th class="has-text-centered">{{ $i++ }}</th>
                    <td>{{ $value->user->registration_number }}</td>
                    <td>{{ $value->user->name }}</td>
                    <td>{{ $value->major->name }}</td>
                    <td>
                        <div class="field is-grouped is-grouped-centered">
                            <a class="button is-warning" href="{{ route('activity.index',['colleger_id'=>$value->id]) }}">
                                <span class="icon">
                                    <i class="fas fa-info" aria-hidden="true"></i>
                                </span>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
      </table>
@endsection
