<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }} {{ app()->version() }}</title>
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <nav class="navbar has-shadow is-dark">
                <div class="container">
                    <div class="navbar-brand">
                        <a href="" class="navbar-item"><img src="{{ asset('img/logo-ums40x40.png') }}"></a>

                        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navMenu">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>

                    <div class="navbar-menu" id="navMenu">
                        <div class="navbar-start">
                            @if (!Auth::guest())
                                @if (Auth::user()->role == 'admin')
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link">
                                        <b>User</b>
                                    </a>
                                    <div class="navbar-dropdown">
                                        <a class="navbar-item" href="{{ route('lecturer.index') }}">
                                            <b>Dosen</b>
                                        </a>
                                        <a class="navbar-item" href="{{ route('colleger.index') }}">
                                            <b>Mahasiswa</b>
                                        </a>
                                    </div>
                                </div>
                                @endif
                            @endif
                        </div>

                        <div class="navbar-end">
                            @if (Auth::guest())
                                <a class="navbar-item " href="{{ route('login') }}"><b>Login</b></a>
                            @else
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#">{{ Auth::user()->name }}</a>

                                    <div class="navbar-dropdown">
                                        <a class="navbar-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </nav>
            <section class="section is-mobile">
                @yield('content')
            </section>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/font.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/modal.js') }}" type="text/javascript"></script>
    </body>
</html>
