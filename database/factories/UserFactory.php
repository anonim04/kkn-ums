<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'role' => 'admin',
        'name' => 'akbar',
        'registration_number' => '12345',
        'email' => 'akbar.tahir89@gmail.com',
        'email_verified_at' => now(),
        'password' => Hash::make('1234567'), // password
        'remember_token' => Str::random(10),
    ];
});
