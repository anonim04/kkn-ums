<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});
Auth::routes();

Route::get('/user/colleger/create', 'CollegerController@create')->name('colleger.create');
Route::post('/user/colleger/store', 'CollegerController@store')->name('colleger.store');

Route::get('/user/lecturer/create', 'LecturerController@create')->name('lecturer.create');
Route::post('/user/lecturer/store', 'LecturerController@store')->name('lecturer.store');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/activity/{colleger_id}/index', 'ActivityController@index')->name('activity.index');
    Route::post('/activity/store', 'ActivityController@store')->name('activity.store');
    Route::put('/activity/{id}/update', 'ActivityController@update')->name('activity.update');
    Route::delete('/activity/{id}/destroy', 'ActivityController@destroy')->name('activity.delete');

    Route::group(['middleware' => ['admin']], function () {
        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/user/lecturer/index', 'LecturerController@index')->name('lecturer.index');
        Route::get('/user/lecturer/{id}/edit', 'LecturerController@edit')->name('lecturer.edit');
        Route::put('/user/lecturer/{id}/update', 'LecturerController@update')->name('lecturer.update');
        Route::delete('/user/lecturer/{id}/destroy', 'LecturerController@destroy')->name('lecturer.delete');

        Route::get('/user/colleger/index', 'CollegerController@index')->name('colleger.index');
        Route::get('/user/colleger/{id}/edit', 'CollegerController@edit')->name('colleger.edit');
        Route::put('/user/colleger/{id}/update', 'CollegerController@update')->name('colleger.update');
        Route::delete('/user/colleger/{id}/destroy', 'CollegerController@destroy')->name('colleger.delete');
    });

    Route::group(['middleware' => ['lecturer']], function () {
        Route::get('/guide/{lecturer_id}/index', 'GuideController@index')->name('guide.index');
        Route::post('/guide/value/store', 'GuideController@store')->name('guide.store');
    });
});

